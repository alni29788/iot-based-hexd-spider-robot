# IOT based Hexd spider robot




## Name
IOT based Hexd spider robot 

## Authors 
Allan Nielsen alni29788@edu.ucl.dk
## License
Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License (CC BY-NC-SA 3.0)

## Project status
The idea of this project was to deliver the product with live video, ultrasonic ranging and other functions. Based on Python3 and PyQt5, it has also been built with server and client, which communicate with each other through TCP/IP protocol and can be controlled remotely with the same LAN.  
